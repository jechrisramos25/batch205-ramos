class Product {

    constructor(name, price){
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive(){ this.isActive = false; }

    updatePrice(newPrice){ 
        (typeof(newPrice) === "number") ? (this.price = newPrice, console.log(`Successfully updated ${this.name}'s price.`)) : console.error(`${this.name}'s new price should be a number.`)
     }

}

const prodA = new Product("product1", 5);
const prodB = new Product("product2", 10);
const prodC = new Product("product3", 5);

class Cart {
    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity){
        let cart = {
            product : (typeof(product) === "object" && product.isActive === true) ? product.name : console.error("Invalid product."),
            price : (typeof(product) === "object" && product.isActive === true) ? product.price : console.error("Invalid product."),
            quantity : (typeof(quantity) === "number" && quantity !== 0) ? quantity : console.error("Product quantity should be a number and should not be 0.")
        }

        if(cart.product !== undefined && cart.quantity !== undefined) {
            this.contents.push(cart);
        }

        return this;
    }

    showCartContents(){
        console.table(this.contents);
        return this;
    }

    clearCartContents(){
        //this.contents.splice(0, this.contents.length);
        this.contents = [];
        this.totalAmount = 0;

        return this;
    }

    computeTotal(){
        let prices = [];
       
        this.contents.forEach( content => prices.push(content.price * content.quantity) );
        
        this.totalAmount = prices.reduce( (accumulator, num) => accumulator += num );
        console.log(`Cart total: $${this.totalAmount}`);
        
        return this;
    }
}

/*let cart1 = new Cart();

cart1.addToCart("prodA", 1);
cart1.addToCart(prodA, "10");

cart1.addToCart(prodA, 1);
cart1.addToCart(prodB, 1);
cart1.addToCart(prodB, 3);

cart1.showCartContents();
cart1.computeTotal();*/

// Mini-Capstone's Stretch-goal

class Customer {
    constructor(email){
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut(){
       if(this.cart.contents.length > 0){
            this.cart.computeTotal();
            let order = {
                products : this.cart.contents,
                totalAmount: this.cart.totalAmount
            }
            this.orders.push(order);
            this.cart.clearCartContents();
       }else{
            console.error(`Unable to checkout because the cart is empty.`)
       }
    }
}

const customer1 = new Customer("name@email.com");

customer1.cart.addToCart(prodA, 1);
customer1.cart.addToCart(prodB, 1);
customer1.checkOut();

console.log(customer1);
