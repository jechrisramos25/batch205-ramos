//QUIZ
/*
    What is the term given to unorganized code that's very hard to work with?
    Spaghetti Code

    How are object literals written in JS?
    {}

    What do you call the concept of organizing information and functionality to belong to an object?
    Encapsulation

    If student1 has a method named enroll(), how would you invoke it?
    student1.enroll();

    True or False: Objects can have objects as properties.
    True.

    What does the this keyword refer to if used in an arrow function method?
    Global Window Object

    True or False: A method can have no parameters and still work.
    True.

    True or False: Arrays can have objects as elements.
    True.

    True or False: Arrays are objects.
    True.

    True or False: Objects can have arrays as properties.
    True


*/
//Function Coding
let student1 = {

    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    login: function (){

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this, when inside a method, refers to the object where it is in.
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve: function (){
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);*/

        //reduce - accumulate through the values of an array and return the accumulated value

        let sum = this.grades.reduce((accumulator,num) => {

            console.log(accumulator);
            //accumulator takes the value of the first item on the first iteration of reduce()

            console.log(num);
            //num takes the value of the next item.

            return accumulator += num;

        })
        //console.log("result of reduce:",sum);
        return sum/4;
    },
    willPass: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 85){
            return true
        } else {
            return false
        }*/

        return Math.round(this.computeAve()) >= 85 ? true : false;
    },
    willPassWithHonors: function() {
/*      let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 90){
            return true
        } else if (Math.round(sum) >= 85 && Math.round(sum) < 90) {
            return false
        } else if(Math.round(sum) < 85){
            return undefined
        }
*/
        return (this.willPass() && Math.round(this.computeAve()) >= 90) ? true : this.willPass() ? false : undefined;
    }

};

let student2 = {

    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],

    login: ()=>{

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this, when inside a method, refers to the object where it is in.
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve: function (){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 85){
            return true
        } else {
            return false
        }*/

        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 90){
            return true
        } else if (Math.round(sum) >= 85 && Math.round(sum) < 90) {
            return false
        } else if(Math.round(sum) < 85){
            return undefined
        }
*/
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }

};

let student3 = {

    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],

    login: ()=>{

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this, when inside a method, refers to the object where it is in.
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve: function (){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 85){
            return true
        } else {
            return false
        }*/

        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 90){
            return true
        } else if (Math.round(sum) >= 85 && Math.round(sum) < 90) {
            return false
        } else if(Math.round(sum) < 85){
            return undefined
        }
*/
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }

};

let student4 = {

    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],

    login: ()=>{

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this, when inside a method, refers to the object where it is in.
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve: function (){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 85){
            return true
        } else {
            return false
        }*/

        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors: function() {
/*        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        sum = sum/4;
        if(Math.round(sum) >= 90){
            return true
        } else if (Math.round(sum) >= 85 && Math.round(sum) < 90) {
            return false
        } else if(Math.round(sum) < 85){
            return undefined
        }
*/
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    },


};