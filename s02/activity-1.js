// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

const student1 = {
    name : 'John',
    email : 'john@mail.com',
    grades : [89, 84, 78, 88],
    login : function() { console.log(`${this.email} has logged in.`); },
    logout : function() { console.log(`${this.email} has logged out.`); },
    listGrades : function () {
        this.grades.forEach( grade => {
            console.log(grade);
        });
    }
}

student1.login();
student1.logout();
student1.listGrades();

const student2 = {
    name : 'Joe',
    email : 'joe@mail.com',
    grades : [78, 82, 79, 85]
}

const student3 = {
    name : 'Jane',
    email : 'jane@mail.com',
    grades : [87, 89, 91, 93]
}

const student4 = {
    name : 'Jessie',
    email : 'jessie@mail.com',
    grades : [91, 89, 92, 93]
}