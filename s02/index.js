/* Quiz
    1. Spaghetti Code
    2. []
    3. Encapsulation
    4. student1.enroll();
    5. 
    6. True
    7. True
    8. True
    9. True
    10. True
*/
// Function coding
const student1 = {
    name : 'John',
    email : 'john@mail.com',
    grades : [89, 84, 78, 88],
    login : function() { console.log(`${this.email} has logged in.`); },
    logout : function() { console.log(`${this.email} has logged out.`); },
    listGrades : function () {
        this.grades.forEach( grade => {
            console.log(grade);
        });
    },
    computeAve : function (){
        /* let gradesRaw = this.grades;
        // let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
        //     return initialGrade+currentGrade;
        // });
        // console.log(total/4); */
        let sum = this.grades.reduce((accumulator, num) => {
            // console.log(accumulator);
            //accumulator takes the value of the first item on the first iteration of reduce()

            // console.log(num);
            //num takes the value of the next item.

            return accumulator += num;
        });
        return sum/4;
    },
    willPass : function () {
       /* let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 85){
            console.log(true);
        }else{
            console.log(false);
        } */
        return Math.round(this.computeAve()) >= 85 ? true : false;
    },
    willPassWithHonors : function() {
        /* let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 90){
            console.log(true);
        }else if(Math.round(total/4) >= 85){
            console.log(false);
        }else{
            console.log(undefined);
        } */
        return (this.willPass() && Math.round(this.computeAve()) >= 90) ? true : this.willPass() ? false : undefined;
    }
}

student1.login();
student1.logout();
student1.listGrades();
student1.computeAve();
student1.willPass();
student1.willPassWithHonors();

const student2 = {
    name : 'Joe',
    email : 'joe@mail.com',
    grades : [78, 82, 79, 85],
    login : function() { console.log(`${this.email} has logged in.`); },
    logout : function() { console.log(`${this.email} has logged out.`); },
    listGrades : function () {
        this.grades.forEach( grade => {
            console.log(grade);
        });
    },
    computeAve : function (){
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        console.log(total/4);
    },
    willPass : function () {
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 85){
            console.log(true);
        }else{
            console.log(false);
        }
    },
    willPassWithHonors : function() {
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 90){
            console.log(true);
        }else if(Math.round(total/4) >= 85){
            console.log(false);
        }else{
            console.log(undefined);
        }
    }
}

student2.login();
student2.logout();
student2.listGrades();
student2.computeAve();
student2.willPass();
student2.willPassWithHonors();

const student3 = {
    name : 'Jane',
    email : 'jane@mail.com',
    grades : [87, 89, 91, 93],
    login : function() { console.log(`${this.email} has logged in.`); },
    logout : function() { console.log(`${this.email} has logged out.`); },
    listGrades : function () {
        this.grades.forEach( grade => {
            console.log(grade);
        });
    },
    computeAve : function (){
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        console.log(total/4);
    },
    willPass : function () {
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 85){
            console.log(true);
        }else{
            console.log(false);
        }
    },
    willPassWithHonors : function() {
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 90){
            console.log(true);
        }else if(Math.round(total/4) >= 85){
            console.log(false);
        }else{
            console.log(undefined);
        }
    }
}

student3.login();
student3.logout();
student3.listGrades();
student3.computeAve();
student3.willPass();
student3.willPassWithHonors();

const student4 = {
    name : 'Jessie',
    email : 'jessie@mail.com',
    grades : [91, 89, 92, 93],
    login : function() { console.log(`${this.email} has logged in.`); },
    logout : function() { console.log(`${this.email} has logged out.`); },
    listGrades : function () {
        this.grades.forEach( grade => {
            console.log(grade);
        });
    },
    computeAve : function (){
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        console.log(total/4);
    },
    willPass : function () {
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 85){
            console.log(true);
        }else{
            console.log(false);
        }
    },
    willPassWithHonors : function() {
        let gradesRaw = this.grades;
        let total =  gradesRaw.reduce((initialGrade, currentGrade) => {
            return initialGrade+currentGrade;
        });
        if(Math.round(total/4) >= 90){
            console.log(true);
        }else if(Math.round(total/4) >= 85){
            console.log(false);
        }else{
            console.log(undefined);
        }
    }
}

student4.login();
student4.logout();
student4.listGrades();
student4.computeAve();
student4.willPass();
student4.willPassWithHonors();