/***** 
1. by using Array literals.
2. 
3. 
4. .indexOf()
5. .forEach()
6. .map()
7. .every()
8. .some()
9. false, should be .slice()
10. true
11. false
12. .math()
13. true
14. false
15. yes
*****/

let students = ["John", "Joe", "Jane", "Jessie"];

// activity #1 - addToEnd
const addToEnd = (array, name) => {
    if(typeof(name) === "string"){
        array.push(name);
        console.log(array);
    }else{
        console.error(`Error ${name} is not a name - can only add strings to an array.`);
    }
}
addToEnd(students, 10);
addToEnd(students, "Carter");

// activity #2 - addToStart
const addToStart = (array, name) => {
    if(typeof(name) === "string"){
        array.unshift(name);
        console.log(array);
    }else{
        console.error(`Error ${name} is not a name - can only add strings to an array.`);
    }
}
addToStart(students, 110);
addToStart(students, "Tess");

// activity #3 - elementChecker
const elementChecker =  (array, name) => {
    if(array.length === 0){
        console.error(`Error: passed in array is empty.`)
    }else if(typeof(name) !== "string"){
        console.error(`Error ${name} is not a name - can only add strings to an array.`);
    }else{
        //return array.some(element => element === name);
        let checkedElements = array.some( person => {
            return name === person;
        });
        // console.log(checkedElements)
        if(checkedElements){
            console.log(`${name} has a match.`);
        }else{
            console.error(`Could not find any matches for ${name}.`);
        }
    }
}
elementChecker([], "Jane");
elementChecker(students, 2000);
elementChecker(students, "Jane");

// activity #4 - stringLengthSorter
const stringLengthSorter = (array) => {
    if(array.length === 0){
        console.error(`Error: passed in array is empty.`);
    }else{
        let elementChecker = array.every( person => {
            return typeof(person) === "string";
        });
        
        if(elementChecker){
            let sortedArray = array.sort((a, b) => a.length - b.length);
            console.log(sortedArray);
        }else{
            console.error(`Error ${array} - all array elements must be strings.`);
        }
    }
}
stringLengthSorter([]);
stringLengthSorter(students);
stringLengthSorter([41, "Bill", 200, "Eddie"]);