let students = [
    "John",
    "James",
    "Jeff",
    "Kevin",
    "David"
];

// mutator array methods
//push - to add a new item at the end of an array
students.push("Henry");
console.log(`new item added at the end ${students}.`);

//unshift - to add a new item at the start of an array
students.unshift("Jeremiah");
console.log(`new item added at the start ${students}.`);

students.unshift("Christine");
console.log(`new item added at the start ${students}.`);

//opposite of push()
students.pop();
console.log(students);

//opposite of unshift()
students.shift();
console.log(students);

//splice() removes and add an item in array from a starting index. Mutator

//slice() copies a portion from a starting index and returns a new array from your portion. Non-Mutator

// iterator methods - loops over the items of an array
// 1- forEach - loops over items in an array and repeats a user-defiend function.
// 2- map() - loops over items in an array and repeats a user-defiend function AND returns a new array.

//every() - loops and checks if all items satisfy a given condition and returns a boolean.

    let arrNum = [15, 25, 50, 20, 10, 11];
    let allDivisible;
    arrNum.forEach( num => (num % 5 === 0) ? console.log(`${num} is divisible by 5`) : allDivisible = false );
    //however, forEach cannot return a data that will tell us if all numbers/items in our arrNum array is divisible by 5
    console.log(allDivisible);
    arrNum.pop();
    arrNum.push(35);
    let divisibleBy5 = arrNum.every( num => {
        //every() is able to return data.
        //when the function inside every() is able to return true, the loop will continue until all items are able to return true.
        //else, if at least one item return false, then the loop will stop there and every() will return false.
        console.log(num);
        return num % 5 === 0;
    });
    console.log(divisibleBy5);

//some() - loops and checks if at least one item satisfies a given condition and returns a boolean. It will return true if at least One item satisfies the condition, else IF no item returns true/satisfies the condition, some() will return false.

    let someDivisibleBy5 = arrNum.some( num => {
        console.log(num);
        return num % 5 === 0;
    });
    console.log(someDivisibleBy5);

    arrNum.push(35);
    let someDivisibleBy11 = arrNum.some(num => {  
        console.log(num);
        return num % 11 === 0;
    });
    console.log(someDivisibleBy11);