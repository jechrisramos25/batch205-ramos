let students = [
    "John",
    "James",
    "Jeff",
    "Kevin",
    "David"
];

console.log(students);

class Dog {
    constructor(name,breed,age){
        this.name = name;
        this.breed = breed;
        this.age = age;
    }
}

let dog1 = new Dog("pomz","corgi",3);
console.log(dog1);

let person1 = {
    name : "Saitama",
    heroName : "One Punch Man",
    age : 30
}
console.log(person1);