class Student {
    
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.average = undefined;
        this.isPassed = undefined;
        this.isPassedWithHonors = undefined;

        if(grades.some(grade => typeof grade !== "number")){
            this.grades = undefined;
        }else{
            this.grades = grades;
        }
    }

    login(){ 
        console.log(`${this.email} has logged in.`);
        return this; 
    }

    logout(){ 
        console.log(`${this.email} has logged out.`);
        return this;
     }

    listGrades(){
        this.grades.forEach( grade => {
            console.log(grade);
        });
        return this;
    }

    helloWorld() {
        console.log(`Hello ${this.name}`);
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator, num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass(){
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors(){
        this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
        return this;
    }
}

//class Section will allow us to group our students as a section.

class Section {

    //every instance of Section class will be instantiated with an empty array for our students.
    constructor(name){
        this.name = name;
        this.students = [];
         //add a counter or number of passed students:
        this.passedStudents = undefined;
        //add a counter or number of honor students:
        this.honorStudents = undefined;
        this.sectionAve = 0;
    }
    //addStudents method will allow us to add instances of the Student class as items for our students property.
    addStudent(name,email,grades){
        //A Student instance/object will be instantiated with the name,email, grades and pushed into our students property.

        this.students.push(new Student(name,email,grades));
        return this;
    }

    //countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors
    countHonorStudents(){
        //accumulate the number of honor students
        let count = 0;
        //loop over each Student instance in the students array
        this.students.forEach(student => {

            //log the student instance currently being iterated.
            //console.log(student);

            //log each student student's isPassedWithHonors property:    
            // console.log(student.willPassWithHonors().isPassedWithHonors);

            //invoke will pass with honors so we can determine and add whether the student passed with honors in its property
            student.willPassWithHonors();

            //isPassedWithHonors property of the student should be populated
            //console.log(student);

            //Check if student IsPassedWithHonor
            //add 1 to a temporary variable to hold the number of honorStudents
            if(student.isPassedWithHonors){
                count++
            };

        });

        //update the honorStudents property with the update value of count:
        this.honorStudents = count;

        return this;
    }
}

/*let section1A = new Section("Section1A");

console.log(section1A);

//3 arguments that are needed to instantiate our Student
section1A.addStudent("Joy","joy@mail.com",[89,91,92,88]);
section1A.addStudent("Jeff","jeff@mail.com",[81,80,82,78]);
section1A.addStudent("John","john@mail.com",[91,90,92,96]);
section1A.addStudent("Jack","jack@mail.com",[95,92,92,93]);

console.log(section1A);

//check the details of John from section1A?
//console.log(section1A.students[2]);

//check the average of John from section1A?
//console.log("John's average:",section1A.students[2].computeAve().average);

//check if Jeff from section1A passed?
console.log("Did Jeff pass?",section1A.students[1].willPass().isPassed);

//display the number of honor students in our section.
//section1A.countHonorStudents();

//check the number of students:
//console.log(section1A.countHonorStudents().honorStudents);*/


/* function coding */
class Grade {
    constructor(level){
        
        // checks if the data type of level is a number
        if(typeof(level) !== "number"){
            this.level = undefined;
        }else{
            this.level = level;
        }
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;

    }

    addSection(name){
        this.sections.push(new Section(name));
        return this;
    }

    countBatchStudents(){
        let count = [];
        this.sections.forEach( section => count.push(section.students.length) );
        this.totalStudents = count.reduce( (accumulator, num) => accumulator += num );
        return this;
    }

    countBatchHonorStudents(){
        let count = [];
        this.sections.forEach( section => {
            count.push(section.countHonorStudents().honorStudents);
        });
        this.totalHonorStudents = count.reduce((accumulator, num) => accumulator += num);
        return this;
    }
}

const grade1 = new Grade(1);
grade1.addSection("Love");
grade1.addSection("Hope");
grade1.addSection("Humility");

grade1.sections[0].addStudent("Anna", "anna@mail.com", [88,89,90,92]);
grade1.sections[0].addStudent("Ben", "ben@mail.com", [89,86,90,85]);
grade1.sections[0].addStudent("Candice", "candice@mail.com", [90,87,88,89]);

grade1.sections[1].addStudent("Alvin", "alvin@mail.com", [89,91,92,88]);
grade1.sections[1].addStudent("Bill", "bill@mail.com", [86,86,87,89]);
grade1.sections[1].addStudent("Charles", "charles@mail.com", [89,87,93,90]);

grade1.sections[2].addStudent("Astrid", "astrid@mail.com", [89,87,87,90]);
grade1.sections[2].addStudent("Ben", "ben@mail.com", [86,88,90,91]);
grade1.sections[2].addStudent("Cooper", "cooper@mail.com", [90,88,89,94]);

grade1.countBatchStudents();
grade1.countBatchHonorStudents();

console.log(grade1);